# Drupal Kubernetes Builder
A docker image used for deploying a Kubernetes cluster, based on `circleci/php:7.1-node-browser` with the following
additions:

- Composer configured correctly
- Drush-launcher, prestissimo and coder pre-installed
- Vim, useful for debugging
- The google cloud cli, azure CLI, kubernetes and helm

## Credits

This is a fork of [Silta-circleci](https://github.com/wunderio/silta-circleci).